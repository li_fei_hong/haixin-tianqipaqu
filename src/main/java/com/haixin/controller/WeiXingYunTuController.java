package com.haixin.controller;

import com.haixin.entity.WeiXingYunTu;
import com.haixin.service.WeiXingYunTuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.controller
 * @ClassName: WeiXingYunTuController
 * @Author: lfh
 * @Description:中央气象台卫星云图
 * @Date: 2020/5/12 11:27
 * @Version: 1.0
 */
@RestController
public class WeiXingYunTuController {
    @Autowired
    private WeiXingYunTuService weiXingYunTuService;

    //查询时间间隔是5分钟
    @RequestMapping("weiXingYunTu01")
    public void WeiXingYunTu01() {
        WeiXingYunTu weiXingYunTu = new WeiXingYunTu(1, 0);
        //FY4A真彩色//FY4A红外//FY4A可见光//FY4A水汽//FY2G可见光
        for (int i = 1; i < 6; i++) {
            weiXingYunTu.setCategory(i);
            weiXingYunTuService.saveWeiXingYunTu01(weiXingYunTu);
        }
    }
    @RequestMapping("weiXingYunTu02")
    public void WeiXingYunTu02() {
        WeiXingYunTu weiXingYunTu = new WeiXingYunTu(6, 1);
        //FY2G增强图下
        for (int i = 1; i <4 ; i++) {
            weiXingYunTu.setType(i);
            weiXingYunTuService.saveWeiXingYunTu02(weiXingYunTu);
        }
        //FY2G黑白图下
        weiXingYunTu.setCategory(7);
        weiXingYunTu.setType(4);//可见光
        weiXingYunTuService.saveWeiXingYunTu02(weiXingYunTu);
        weiXingYunTu.setType(5);//中红外
        weiXingYunTuService.saveWeiXingYunTu02(weiXingYunTu);
        //FY2G圆盘图下
        weiXingYunTu.setCategory(8);
        for (int i = 6; i <12 ; i++) {
            weiXingYunTu.setType(i);
            weiXingYunTuService.saveWeiXingYunTu02(weiXingYunTu);
        }

    }


}
