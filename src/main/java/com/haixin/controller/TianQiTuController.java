package com.haixin.controller;

import com.haixin.entity.TianQiTu;
import com.haixin.service.TianQiTuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.controller
 * @ClassName: TinaQiTuController
 * @Author: lfh
 * @Description:中央气象台天气图
 * @Date: 2020/5/11 14:59
 * @Version: 1.0
 */
@RestController
public class TianQiTuController {
    @Autowired
    private TianQiTuService tianQiTuService;

    //地面层次图,时间间隔为3小时更新
    @RequestMapping("diMianTu01")
    public void diMianTu() {
        TianQiTu tianQiTu = new TianQiTu(1,1,1);
       tianQiTuService.saveDiMianTu(tianQiTu);//      中国  地面基本图片
        tianQiTu.setArea(2);
        tianQiTuService.saveDiMianTu(tianQiTu);//        亚欧 地面基本图片
        tianQiTu.setArea(3);
        tianQiTuService.saveDiMianTu(tianQiTu);//        北半球  地面基本天气
        tianQiTu.setArea(1);
        tianQiTu.setType(2);
        tianQiTuService.saveDiMianTu(tianQiTu);//        中国  地面叠加卫星图片
        tianQiTu.setArea(2);
        tianQiTuService.saveDiMianTu(tianQiTu);//        亚欧  地面叠加卫星图片
        tianQiTu.setArea(1);
        tianQiTu.setType(3);
        tianQiTuService.saveDiMianTu(tianQiTu);//        中国  地面叠加雷达图片
    }

    //非地面层次图,时间间隔为12小时更新
    @RequestMapping("diMianTu02")
    public void noDiMinnTu() {
        TianQiTu tianQiTu = new TianQiTu();
        //根据http://www.nmc.cn/publish/observations/north/dm/weatherchart-h000.htm天气图非地面图规则遍历
        for (int area=3;area>0;area--){
            tianQiTu.setArea(area);
            for (int type=area;type>0;type--){
                tianQiTu.setType(type);
                for (int level=2;level<8;level++){
                    tianQiTu.setLevel(level);
                    tianQiTuService.saveNoDiMianTu(tianQiTu);
                }
            }
        }
    }


}
