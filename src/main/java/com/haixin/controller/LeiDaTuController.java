package com.haixin.controller;

import com.haixin.entity.TianQiTu;
import com.haixin.service.LeiDaTuService;
import com.haixin.service.TianQiTuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.controller
 * @ClassName: TinaQiTuController
 * @Author: lfh
 * @Description:中央气象台雷达图
 * @Date: 2020/5/11 14:59
 * @Version: 1.0
 */
@RestController
public class LeiDaTuController {
    @Autowired
    private LeiDaTuService leiDaTuService;

    //地面层次图,时间间隔为3小时更新
    @RequestMapping("leiDaTu")
    public void diMianTu() {
        leiDaTuService.saveLeiDaTu();
    }

}
