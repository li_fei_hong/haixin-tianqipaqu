package com.haixin.entity;

import lombok.Data;

import java.util.Date;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.entity
 * @ClassName: LeiDaTu
 * @Author: lfh
 * @Description: 雷达图
 * @Date: 2020/5/14 11:24
 * @Version: 1.0
 */
@Data
public class LeiDaTu {
    private Integer id;//编号
    private Date createDate;//创建时间
    private String pic;//图片路径
}
