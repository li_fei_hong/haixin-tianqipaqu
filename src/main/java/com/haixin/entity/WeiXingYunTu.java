package com.haixin.entity;

import lombok.Data;

import java.util.Date;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.entity
 * @ClassName: WeiXingYunTu
 * @Author: lfh
 * @Description: 卫星云图
 * @Date: 2020/5/12 11:30
 * @Version: 1.0
 */
@Data
public class WeiXingYunTu {
    public WeiXingYunTu() {
    }

    public WeiXingYunTu(Integer category, Integer type) {
        this.category = category;
        this.type = type;
    }

    private Integer id;//主键
    private Date createDate;//创建时间
    private String pic;//图片路径
    private Integer category;//类别
    private Integer type;//类型
}
