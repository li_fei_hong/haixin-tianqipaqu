package com.haixin.entity;

import lombok.Data;

import java.util.Date;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.entity
 * @ClassName: WeatherTianQiTu
 * @Author: lfh
 * @Description:天气图
 * @Date: 2020/5/11 15:27
 * @Version: 1.0
 */
@Data
public class TianQiTu {

    public TianQiTu() {
    }

    public TianQiTu(Integer area, Integer level, Integer type) {
        this.area = area;
        this.level = level;
        this.type = type;
    }

    private Integer id;//编号
    private Date createDate;//创建时间
    private String pic;//图片路径
    private Integer area;//区域
    private Integer level;//层次
    private Integer type;//类型
}
