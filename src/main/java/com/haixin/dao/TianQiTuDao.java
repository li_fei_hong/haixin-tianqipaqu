package com.haixin.dao;

import com.haixin.entity.TianQiTu;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.dao
 * @ClassName: WeatherTianQiTuMapper
 * @Author: lfh
 * @Description:
 * @Date: 2020/5/11 15:31
 * @Version: 1.0
 */
@Mapper
@Repository
public interface TianQiTuDao {
    //获取上次存储图片时间
    @Select("select create_date createDate from weather_tqt where area=#{area} and level=#{level} and type =#{type} ORDER BY id desc LIMIT 1")
    public TianQiTu getOneTianQiTuDate(TianQiTu tianQiTu);

    //保存图片路径
    @Insert("insert into weather_tqt values(0,#{createDate},#{pic},#{area},#{level},#{type})")
    public Integer save(TianQiTu tianQiTu);
}
