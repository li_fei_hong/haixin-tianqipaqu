package com.haixin.dao;

import com.haixin.entity.WeiXingYunTu;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.dao
 * @ClassName: WeiXingYunTuDao
 * @Author: lfh
 * @Description:数据库sql语句
 * @Date: 2020/5/12 14:42
 * @Version: 1.0
 */
@Mapper
@Repository
public interface WeiXingYunTuDao {
    //获取上次存储图片时间
    @Select("select create_date createDate from weather_wxyt where category=#{category} and  type=#{type} ORDER BY id desc LIMIT 1")
    public WeiXingYunTu getOneWeiXingYunTuDate(WeiXingYunTu weiXingYunTu);
    //保存图片路径
    @Insert("insert into weather_wxyt values(0,#{createDate},#{pic},#{category},#{type})")
    public Integer save(WeiXingYunTu weiXingYunTu);
}
