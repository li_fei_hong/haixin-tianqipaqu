package com.haixin.dao;

import com.haixin.entity.LeiDaTu;
import com.haixin.entity.TianQiTu;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.dao
 * @ClassName: LeiDaTuDao
 * @Author: lfh
 * @Description:
 * @Date: 2020/5/14 10:59
 * @Version: 1.0
 */
@Mapper
@Repository
public interface LeiDaTuDao {

    @Select("select create_date createDate from weather_ldt ORDER BY id desc LIMIT 1")
    public LeiDaTu getOneLeiDaTuDate();

    //保存图片路径
    @Insert("insert into weather_ldt values(0,#{createDate},#{pic})")
    public Integer save(LeiDaTu leiDaTu);
}
