package com.haixin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @ProjectName: haixi-ntianqipaqu
 * @Package: com.haixin
 * @ClassName: StartApp
 * @Author: lfh
 * @Description:
 * @Date: 2020/5/11 12:26
 * @Version: 1.0
 */
@SpringBootApplication
@EnableScheduling
public class StartApp {
    public static void main(String[] args) {
        SpringApplication.run(StartApp.class,args);
    }
}
