package com.haixin.service;

import com.haixin.entity.TianQiTu;
import com.haixin.entity.WeiXingYunTu;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.service
 * @ClassName: WeiXingYunTuService
 * @Author: lfh
 * @Description:
 * @Date: 2020/5/12 11:29
 * @Version: 1.0
 */
public interface WeiXingYunTuService {

    //查询:FY4A真彩色,FY4A红外,FY4A可见光,FY4A水汽,FY2G可见
    //时间间隔每分钟
    public void saveWeiXingYunTu01(WeiXingYunTu weiXingYunTu);
    //查询:FY2G增强图,FY2G黑白图,FY2G圆盘图
    //时间间隔1小时
    public void saveWeiXingYunTu02(WeiXingYunTu weiXingYunTu);
}
