package com.haixin.service;

import com.haixin.entity.TianQiTu;

import java.io.IOException;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.service
 * @ClassName: TianQiTuService
 * @Author: lfh
 * @Description:
 * @Date: 2020/5/11 15:39
 * @Version: 1.0
 */
public interface TianQiTuService {
//天气图地面层次图,三小时间隔
public void saveDiMianTu(TianQiTu tianQiTu);

//天气图非地面层次图,12小时间隔
    public void saveNoDiMianTu(TianQiTu tianQiTu);

}
