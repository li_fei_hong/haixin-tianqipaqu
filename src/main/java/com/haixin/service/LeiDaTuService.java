package com.haixin.service;

import com.haixin.entity.LeiDaTu;
import com.haixin.entity.TianQiTu;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.service
 * @ClassName: TianQiTuService
 * @Author: lfh
 * @Description: 雷达图
 * @Date: 2020/5/11 15:39
 * @Version: 1.0
 */
public interface LeiDaTuService {
//雷达拼图/全国,六分钟间隔
public void saveLeiDaTu();
}
