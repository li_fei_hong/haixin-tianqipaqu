package com.haixin.service.impl;

import com.haixin.dao.TianQiTuDao;
import com.haixin.entity.TianQiTu;
import com.haixin.service.TianQiTuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import sun.java2d.cmm.Profile;

import javax.xml.crypto.Data;
import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.service.impl
 * @ClassName: TianQiTuServiceImpl
 * @Author: lfh
 * @Description:
 * @Date: 2020/5/11 15:39
 * @Version: 1.0
 */
@Service
public class TianQiTuServiceImpl implements TianQiTuService {
    @Autowired
    private TianQiTuDao tianQiTuDao;
    @Value("${file.path}")
    private String path;//图片保存路径
    @Value("${file.domain}")
    private String url;

    @Override
//    天气图的地面层次图,三小时间隔
    public void saveDiMianTu(TianQiTu tianQiTu) {
        String area;
        String type;
        if (tianQiTu.getArea() == 1) {
            area = "ACWP";//中国标识
        } else if (tianQiTu.getArea() == 2) {
            area = "ACHN";//亚欧标识
        } else {
            area = "ANHE";//北半球标识
        }
        if (tianQiTu.getType() == 1) {
            type = "EGH";//基本图片标识
        } else if (tianQiTu.getType() == 2) {
            type = "ESPCT";//叠加图片标识
        } else {
            type = "ESPBT";//叠加雷达标识
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHH");//字符串替换格式
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy/MM/dd");//字符串替换格式,查找的日期
        TianQiTu oneTianQiTu = tianQiTuDao.getOneTianQiTuDate(tianQiTu);// 获取数据库上次更新时间
        Long a = null;
        Date date1 = null;
        if (oneTianQiTu != null) {//根据数据库之前保存接着来
            date1 = new Date();
//            数据库存储的时间戳
            date1.setTime(oneTianQiTu.getCreateDate().getTime());
            a = oneTianQiTu.getCreateDate().getTime();
        } else {//数据库没有数据则从当天开始
            Calendar cal1 = Calendar.getInstance();
            //设置小时为2小时
            cal1.set(Calendar.HOUR_OF_DAY, 2);
            date1 = cal1.getTime();
            a = cal1.getTime().getTime();
        }
        //时间加三小时,查找具体时间由于中国时间和utc时间差8小时,所以减5小时
        for (a = a - 5 * 60 * 60 * 1000; a < new Date().getTime() - 8 * 60 * 60 * 1000; a += 3 * 60 * 60 * 1000) {
            date1.setTime(date1.getTime() + 3 * 60 * 60 * 1000);
            tianQiTu.setCreateDate(date1);
            //根据format2格式生成字符串
            String format3 = format2.format(a);
            String format1 = format.format(a);
            String url = "http://image.nmc.cn/product/" + format3 + "/WESA/SEVP_NMC_WESA_SFER_" + type + "_" + area + "_L00_P9_" + format1 + "0000000.png";
            //保存图片日期+3小时到数据库
            this.savePic(tianQiTu, url, format3);
        }
    }

    @Override
    public void saveNoDiMianTu(TianQiTu tianQiTu) {
        String area;
        String type;
        String level;
        //编辑层级
        if (tianQiTu.getLevel() == 2) {
            level = "L92_P9_";
        } else if (tianQiTu.getLevel() == 3) {
            level = "L85_P9_";
        } else if (tianQiTu.getLevel() == 4) {
            level = "L70_P9_";
        } else if (tianQiTu.getLevel() == 5) {
            level = "L50_P9_";
        } else if (tianQiTu.getLevel() == 6) {
            level = "L20_P9_";
        } else {
            level = "L10_P9_";
        }
        if (tianQiTu.getArea() == 1) {
            area = "ACWP_";//中国标识
        } else if (tianQiTu.getArea() == 2) {
            area = "ACHN_";//亚欧标识
        } else {
            area = "ANHE_";//北半球标识
        }
        if (tianQiTu.getType() == 1) {
            type = "EGH_";//基本图片标识
        } else if (tianQiTu.getType() == 2) {
            type = "ESPCT_";//叠加图片标识
        } else {
            type = "ESPBT_";//叠加雷达标识
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHH");//字符串替换格式
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy/MM/dd");//字符串替换格式,查找的日期
        Long a = null;
        Date date1 = null;
//        获取上次数据库更新时间
        TianQiTu oneTianQiTu = tianQiTuDao.getOneTianQiTuDate(tianQiTu);
        if (oneTianQiTu != null) {//根据数据库之前保存接着来
            date1 = new Date();
//            数据库存储的时间戳
            date1.setTime(oneTianQiTu.getCreateDate().getTime());
            a=oneTianQiTu.getCreateDate().getTime();
        } else {//数据库没有数据则从当天开始
            Calendar cal1 = Calendar.getInstance();
            //设置小时为0小时
            cal1.set(Calendar.HOUR_OF_DAY, 0);
             date1 = cal1.getTime();
             a=cal1.getTime().getTime();
        }
        //时间加12小时,查找具体时间由于中国时间和utc时间差8小时,所以加4小时
        for ( a = a + 4 * 60 * 60 * 1000; a < new Date().getTime() - 12 * 60 * 60 * 1000; a += 12 * 60 * 60 * 1000) {
            date1.setTime(date1.getTime() + 12 * 60 * 60 * 1000);
            tianQiTu.setCreateDate(date1);
            //根据format2格式生成字符串
            String format3 = format2.format(a);
            String format1 = format.format(a);
            String url = "http://image.nmc.cn/product/" + format3 + "/WESA/SEVP_NMC_WESA_SFER_" + type + area + level + format1 + "0000000.png";
            //保存图片日期+3小时到数据库
            this.savePic(tianQiTu, url, format3);
        }
    }


    //图片保存本地,信息保存数据库
    public void savePic(TianQiTu tianQiTu, String url, String dateFormat) {
        System.out.println(url);
        DataInputStream dataInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            URLConnection urlConnection = new URL(url).openConnection();
            urlConnection.setConnectTimeout(2000);//是建立连接的超时时间；
            urlConnection.setReadTimeout(1000);//是传递数据的超时时间。
            dataInputStream = new DataInputStream(urlConnection.getInputStream());
            int i = url.lastIndexOf("/");//获取图片名称
            String substring = url.substring(i);
            File file = new File(path + dateFormat);
            if (!file.exists()) {//判断文件夹是否存在
                file.mkdirs();//创建文件夹
            }
            fileOutputStream = new FileOutputStream(path + dateFormat + substring);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;

            while ((length = dataInputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, length);
            }
            dataInputStream.close();
            fileOutputStream.close();
            tianQiTu.setPic(dateFormat + substring);//保存路径
            tianQiTuDao.save(tianQiTu);
        } catch (Exception e) {
            System.out.println("该时间段图片不存在");
        }

    }
}
