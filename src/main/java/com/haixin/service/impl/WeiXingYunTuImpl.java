package com.haixin.service.impl;

import com.haixin.dao.WeiXingYunTuDao;
import com.haixin.entity.TianQiTu;
import com.haixin.entity.WeiXingYunTu;
import com.haixin.service.WeiXingYunTuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.service.impl
 * @ClassName: WeiXingYunTuImpl
 * @Author: lfh
 * @Description:
 * @Date: 2020/5/12 11:29
 * @Version: 1.0
 */
@Service
public class WeiXingYunTuImpl implements WeiXingYunTuService {
    @Value("${file.path}")
    private String path;//图片保存路径
    @Value("${file.domain}")
    private String url;
    @Autowired
    private WeiXingYunTuDao weiXingYunTuDao;

    //查询间隔一分钟,每五分钟运行一次
    //category值为1-5
    //type值为1-5
    @Override
    public void saveWeiXingYunTu01(WeiXingYunTu weiXingYunTu) {
        String category;//类别
        String path;//图片拼接路径
        if (weiXingYunTu.getCategory() == 5) {
            category = "CORN_FY2G_";
            path="/CORN";
        } else {
            category = "WXBL_FY4A_";
            path="/WXBL";
        }
        if (weiXingYunTu.getCategory() == 1) {
            category += "ETCC";
        } else if (weiXingYunTu.getCategory() == 2) {
            category += "EC012";
        } else if (weiXingYunTu.getCategory() == 3) {
            category += "EC001";
        } else if (weiXingYunTu.getCategory() == 4) {
            category += "EC009";
        } else {
            category += "EVL";
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");//字符串替换格式
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy/MM/dd");//字符串替换格式,查找的日期
        Long a = null;
        Date date1 = null;
//        获取数据库上次存储时间
        WeiXingYunTu oneWeiXingYunTuDate = weiXingYunTuDao.getOneWeiXingYunTuDate(weiXingYunTu);
        if (oneWeiXingYunTuDate != null) {//根据数据库之前保存接着查询
             date1 = new Date();
//            数据库存储的时间戳
            date1.setTime(oneWeiXingYunTuDate.getCreateDate().getTime());
            a=oneWeiXingYunTuDate.getCreateDate().getTime();
        } else {//数据库没有数据则从当天开始
            Calendar cal1 = Calendar.getInstance();
            //设置小时为0
            cal1.set(Calendar.HOUR_OF_DAY, 0);
            //设置分钟为0
            cal1.set(Calendar.MINUTE, 0);
             date1 = cal1.getTime();
             a= cal1.getTime().getTime();
        }
        //时间加一分钟,查找具体时间由于中国时间和utc时间差8小时,所以减8小时
        for (a = a - 8 * 60 * 60 * 1000 + 1000 * 60; a < new Date().getTime() - 8 * 60 * 60 * 1000; a += 60 * 1000) {
            date1.setTime(date1.getTime() + 60 * 1000);
            weiXingYunTu.setCreateDate(date1);
            //根据format2格式生成字符串
            String format3 = format2.format(a);
            String format1 = format.format(a);
            String url = "http://image.nmc.cn/product/" + format3 +path+"/medium/SEVP_NSMC_" + category + "_ACHN_LNO_PY_" + format1 + "00000.JPG";
            //保存图片日期+3小时到数据库
            this.savePic(weiXingYunTu, url, format3);
        }
    }

    @Override
    public void saveWeiXingYunTu02(WeiXingYunTu weiXingYunTu) {
        String category;//类别
        String  type;//类型
        if (weiXingYunTu.getCategory() == 8) {
            category = "WXBL";
        } else {
            category = "WXCL";
        }
        if (weiXingYunTu.getType() == 1 || weiXingYunTu.getType() == 10) {
            type = "_FY2G_EWVP";
        }  else if (weiXingYunTu.getType() == 2 || weiXingYunTu.getType() == 7) {
            type = "_FY2G_EIR1";
        } else if (weiXingYunTu.getType() == 3 || weiXingYunTu.getType() == 8) {
            type = "_FY2G_EIR2";
        } else if (weiXingYunTu.getType() == 4 || weiXingYunTu.getType() == 11) {
            type = "_FY2G_EVL";
        } else if (weiXingYunTu.getType() == 5 || weiXingYunTu.getType() == 9) {
            type = "_FY2G_EMIR";
        } else {
            type = "_FY2G_ECN";
        }
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHH");//字符串替换格式
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy/MM/dd");//字符串替换格式,查找的日期
        Long a = null;
        Date date1 = null;
//        获取数据库上次存储时间
        WeiXingYunTu oneWeiXingYunTuDate = weiXingYunTuDao.getOneWeiXingYunTuDate(weiXingYunTu);
        if (oneWeiXingYunTuDate != null) {//根据数据库之前保存接着查询
            date1 = new Date();
//            数据库存储的时间戳
            date1.setTime(oneWeiXingYunTuDate.getCreateDate().getTime());
            a=oneWeiXingYunTuDate.getCreateDate().getTime();
        } else {//数据库没有数据则从当天开始
            Calendar cal1 = Calendar.getInstance();
            //设置小时为0
            cal1.set(Calendar.HOUR_OF_DAY, 0);
            //设置分钟为0
            cal1.set(Calendar.MINUTE, 0);
            date1 = cal1.getTime();
            a= cal1.getTime().getTime();
        }
        //时间加一小时,查找具体时间由于中国时间和utc时间差8小时,所以减7小时
        for (a = a - 7 * 60 * 60 * 1000 ; a < new Date().getTime() - 8 * 60 * 60 * 1000; a += 60 * 60 * 1000) {
            date1.setTime(date1.getTime() +60 * 60 * 1000);
            weiXingYunTu.setCreateDate(date1);
            //根据format2格式生成字符串
            String format3 = format2.format(a);
            String format1 = format.format(a);
            String url = "http://image.nmc.cn/product/"+format3+"/"+category+"/medium/SEVP_NSMC_"+category+type+"_ACHN_LNO_PY_"+format1+"0000000.JPG";
            //保存图片日期+3小时到数据库
            this.savePic(weiXingYunTu, url, format3);
        }
    }
//    保存图片到本地并更新数据库
    public void savePic(WeiXingYunTu weiXingYunTu, String url, String dateFormat) {
        System.out.println(url);
        DataInputStream dataInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            URLConnection urlConnection = new URL(url).openConnection();
            urlConnection.setConnectTimeout(2000);//是建立连接的超时时间；
            urlConnection.setReadTimeout(1000);//是传递数据的超时时间。
            dataInputStream = new DataInputStream(urlConnection.getInputStream());
            int i = url.lastIndexOf("/");//获取图片名称
            String substring = url.substring(i);
            File file = new File(path + dateFormat);
            if (!file.exists()) {//判断文件夹是否存在
                file.mkdirs();//创建文件夹
            }
            fileOutputStream = new FileOutputStream(path + dateFormat + substring);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;

            while ((length = dataInputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, length);
            }
            dataInputStream.close();
            fileOutputStream.close();
            weiXingYunTu.setPic(dateFormat + substring);//保存路径
            weiXingYunTuDao.save(weiXingYunTu);
        } catch (Exception e) {
            System.out.println("该时间段图片不存在");
        }

    }
}
