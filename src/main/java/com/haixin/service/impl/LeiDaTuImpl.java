package com.haixin.service.impl;

import com.haixin.dao.LeiDaTuDao;
import com.haixin.dao.WeiXingYunTuDao;
import com.haixin.entity.LeiDaTu;
import com.haixin.entity.TianQiTu;
import com.haixin.entity.WeiXingYunTu;
import com.haixin.service.LeiDaTuService;
import com.haixin.service.WeiXingYunTuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @ProjectName: haixin-tianqipaqu
 * @Package: com.haixin.service.impl
 * @ClassName: WeiXingYunTuImpl
 * @Author: lfh
 * @Description:
 * @Date: 2020/5/12 11:29
 * @Version: 1.0
 */
@Service
public class LeiDaTuImpl implements LeiDaTuService {
    @Value("${file.path}")
    private String path;//图片保存路径
    @Value("${file.domain}")
    private String url;
    @Autowired
    private LeiDaTuDao leiDaTuDao;

    //查询间隔一分钟,每五分钟运行一次
    //category值为1-5
    //type值为1-5
    @Override
    public void saveLeiDaTu() {
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");//字符串替换格式
        SimpleDateFormat format2 = new SimpleDateFormat("yyyy/MM/dd");//字符串替换格式,查找的日期
        Long a = null;
        Date date1 = null;
//        获取数据库上次存储时间
        LeiDaTu leiDaTu = leiDaTuDao.getOneLeiDaTuDate();
        if (leiDaTu != null) {//根据数据库之前保存接着查询
             date1 = new Date();
//            数据库存储的时间戳
            date1.setTime(leiDaTu.getCreateDate().getTime());
            a=leiDaTu.getCreateDate().getTime();
        } else {//数据库没有数据则从当天开始
            Calendar cal1 = Calendar.getInstance();
            //设置小时为0
            cal1.set(Calendar.HOUR_OF_DAY, 0);
            //设置分钟为0
            cal1.set(Calendar.MINUTE, 0);
             date1 = cal1.getTime();
             a= cal1.getTime().getTime()-1000 * 60 * 6;
        }
        //时间加6分钟,查找具体时间由于中国时间和utc时间差8小时,所以减8小时
        for (a = a - 8 * 60 * 60 * 1000 + 1000 * 60 * 6; a < new Date().getTime() - 8 * 60 * 60 * 1000; a += 6 * 60 * 1000) {
            date1.setTime(date1.getTime() +6 * 60 * 1000);
            leiDaTu.setCreateDate(date1);
            //根据format2格式生成字符串
            String format3 = format2.format(a);
            String format1 = format.format(a);
            String url = "http://image.nmc.cn/product/"+format3+"/RDCP/medium/SEVP_AOC_RDCP_SLDAS_EBREF_ACHN_L88_PI_"+format1+"00001.PNG";
            //保存图片日期+3小时到数据库
            this.savePic(leiDaTu, url, format3);
        }
    }

//    保存图片到本地并更新数据库
    public void savePic(LeiDaTu leiDaTu, String url, String dateFormat) {
        System.out.println(url);
        DataInputStream dataInputStream = null;
        FileOutputStream fileOutputStream = null;
        try {
            URLConnection urlConnection = new URL(url).openConnection();
            urlConnection.setConnectTimeout(2000);//是建立连接的超时时间；
            urlConnection.setReadTimeout(1000);//是传递数据的超时时间。
            dataInputStream = new DataInputStream(urlConnection.getInputStream());
            int i = url.lastIndexOf("/");//获取图片名称
            String substring = url.substring(i);
            File file = new File(path + dateFormat);
            if (!file.exists()) {//判断文件夹是否存在
                file.mkdirs();//创建文件夹
            }
            fileOutputStream = new FileOutputStream(path + dateFormat + substring);
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            byte[] buffer = new byte[1024];
            int length;

            while ((length = dataInputStream.read(buffer)) > 0) {
                fileOutputStream.write(buffer, 0, length);
            }
            dataInputStream.close();
            fileOutputStream.close();
            leiDaTu.setPic(dateFormat + substring);//保存路径
            leiDaTuDao.save(leiDaTu);
        } catch (Exception e) {
            System.out.println("该时间段图片不存在");
        }

    }
}
