package com.haixin;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import java.time.LocalDateTime;

/**
 * @ProjectName: haixi-ntianqipaqu
 * @Package: com.haixin
 * @ClassName: paqu
 * @Author: lfh
 * @Description:
 * @Date: 2020/5/11 12:22
 * @Version: 1.0
 */
@Controller
public class PaQuTest {
    //3.添加定时任务
//    @Scheduled(cron = "0/5 * * * * ?")
    //或直接指定时间间隔，例如：5秒
    //@Scheduled(fixedRate=5000)
    private void configureTasks() {
        System.err.println("执行静态定时任务时间: " + LocalDateTime.now());
    }

}
