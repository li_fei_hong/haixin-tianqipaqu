create table weather_tqt(
id int PRIMARY key auto_increment comment '主键',
create_date DATETIME not null comment '图片对应时间气象',
pic varchar(120) not null comment '图片路径',
area int not null comment '区域,1代表中国,2代表亚欧,3代表北半球',
level int not null comment '层次,1代表地面,2代表925hPa,3代表850hPa,4代表700hPa,5代表500hPa,6代表200hPa,7代表100hPa',
type int not null comment '类型,1代表基本天气分析,2代表叠加卫星云图,3代表叠加雷达拼图'
) comment '天气图,如果手动设置数据,需要根据项目设置对应时间节点';

create table weather_wxyt(
id int PRIMARY key auto_increment comment '主键',
create_date DATETIME not null comment '图片对应时间气象',
pic varchar(120) not null comment '图片路径',
category int not null comment '类别,1代表FY4A真彩色,2代表FY4A红外,3代表FY4A可见光,4代表FY4A水汽,5代表FY2G可见光,6代表FY2G增强图,7代表FY2G黑白图,8代表FY2G圆盘图',
type int not null comment '类型,0代表没有,1:增强图/水汽,2:增强图/红外一,3:增强图/红外二,4:黑白图/可见光,5:黑白图/中红外,6:圆盘图/彩色圆盘图,7:
圆盘图/8:红外一圆盘图,8:圆盘图/红外二圆盘图,10:圆盘图/中红外圆盘图,11:圆盘图/水汽圆盘图,12:圆盘图/可见光圆盘图'
) comment '卫星云图,如果手动设置数据,需要根据项目设置对应时间节点';


EXPLAIN select create_date from weather_wxyt where category=1 and  type=0 ORDER BY id desc LIMIT 1;

EXPLAIN  select create_date from weather_tqt where area=1 and level=1 and type =1 ORDER BY id desc LIMIT 1;